from django.db.models import fields
from authApp.models.pedido import Pedido
from rest_framework import serializers

class PedidoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pedido
        fields = [ 'email_c','numero_factura','estado','costoUnitario','forma_pago','estado_pago']
        
    def to_representation(self, obj):        
        return {
            'id': obj.id,
            'email_c': obj.email_c.email,
            'numero_factura': obj.numero_factura,
            'estado': obj.estado,
            'costoUnitario': obj.costoUnitario,
            'forma_pago': obj.forma_pago,
            'estado_pago': obj.estado_pago,
        }