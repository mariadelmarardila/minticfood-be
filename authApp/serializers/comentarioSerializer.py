from rest_framework import serializers
from authApp.models.comentario import Comentario

from authApp.models.user import User

class ComentarioSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comentario        
        fields = ['email_c','comentario','califica_producto','califica_embalaje','califica_tiempo_entrega']

    def to_representation(self, obj):        
        return {
            'id_comentario': obj.id_comentario,
            'email_c': obj.email_c.email,
            'comentario': obj.comentario,
            'califica_producto': obj.califica_producto,
            'califica_embalaje': obj.califica_embalaje,
            'califica_tiempo_entrega': obj.califica_tiempo_entrega,
        }
