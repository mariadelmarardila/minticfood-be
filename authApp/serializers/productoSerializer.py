from rest_framework import serializers
from authApp.models.producto import Producto


class ProductoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Producto
        fields = ['id', 'nombre', 'descripcion',
                  'costoUnitario', 'presentacion', 'disponibilidad']

    def to_representation(self, obj):        
        return {
            'id': obj.id,
            'nombre': obj.nombre,
            'descripcion': obj.descripcion,
            'costoUnitario': obj.costoUnitario,
            'presentacion': obj.presentacion,
            'disponibilidad': obj.disponibilidad,
        }
