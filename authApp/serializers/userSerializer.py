from rest_framework import serializers
from authApp.models.user import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'username','password', 'nombre','email','edad','direccion','telefono','ciudad','grupo','fecha_reg']
    
    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance
    
    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        return {
            'id': user.id,
            'username': user.username,
            'password': user.password,
            'nombre': user.nombre,
            'email': user.email,
            'edad': user.edad,
            'direccion': user.direccion,
            'telefono': user.telefono,
            'ciudad': user.ciudad,
            'grupo': user.grupo,
            'fecha_reg': user.fecha_reg 
        }