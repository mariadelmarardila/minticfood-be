from django.db import models
from django.db.models.fields.related import ForeignKey
from .user import User
class Comentario(models.Model):
    id_comentario = models.AutoField(primary_key=True)
    email_c=models.ForeignKey(User,related_name='comentario',on_delete=models.CASCADE)
    comentario = models.TextField()
    califica_producto = models.IntegerField(blank=False,null=False)
    califica_embalaje = models.DecimalField(max_digits=10,decimal_places=2)
    califica_tiempo_entrega = models.IntegerField(blank=True,null=True)
    