from django.db import models
from django.db.models.fields.related import ForeignKey
from .user import User

class Pedido(models.Model):
    #id_pedido = models.AutoField(primary_key=True)
    email_c=models.ForeignKey(User,related_name='pedido',on_delete=models.CASCADE)
    numero_factura = models.CharField(max_length = 12)
    estado = models.IntegerField(blank=False,null=False)
    costoUnitario = models.DecimalField(max_digits=10,decimal_places=2)
    forma_pago = models.IntegerField(blank=True,null=True)
    estado_pago = models.BooleanField(blank=False,null=False,default=False)



