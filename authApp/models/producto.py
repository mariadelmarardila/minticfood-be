from django.db import models

class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length = 400)
    descripcion = models.CharField(max_length = 5000)
    costoUnitario = models.DecimalField(max_digits=10,decimal_places=2)
    presentacion = models.CharField(max_length = 5000)
    disponibilidad = models.BooleanField(default=True)



