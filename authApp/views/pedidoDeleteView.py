from rest_framework import generics,status
from authApp.models.pedido import Pedido
from django.db.models.query import QuerySet
from rest_framework.response import Response

class PedidoDeleteView(generics.RetrieveAPIView):
    def delete (self,request,pk=None,*args,**kwargs):
        queryset= Pedido.objects.filter(id_pedido=pk).first()
        if queryset:
            queryset.delete()
            return Response({'message':'Se elimino el Pedido'},status=status.HTTP_200_OK)
        return Response({'message':'No se ha encontrado el Pedido'},status=status.HTTP_400_BAD_REQUEST)
            