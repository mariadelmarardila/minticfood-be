from rest_framework import status, views
from rest_framework.response import Response
from authApp.serializers.comentarioSerializer import ComentarioSerializer


class ComentarioCreateView(views.APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = ComentarioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Comentario creado correctamente'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    