#from django.conf import settings
from django.db.models.query import QuerySet
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer


class UserUpdateView(generics.RetrieveAPIView):   

    def put(self,request,pk=None, *args, **kwargs, ):            
        queryset = User.objects.filter(username = pk).first()      
        user_serializer = UserSerializer(queryset,data=request.data)
        
        if queryset:
            if user_serializer.is_valid():
                user_serializer.save()
                return Response(user_serializer.data, status=status.HTTP_200_OK)        
            return Response(user_serializer.errors, status=status.HTTP_400_BAD_REQUEST)        
        return Response({'message':'No se ha encontrado el usuario'}, status=status.HTTP_400_BAD_REQUEST)        
