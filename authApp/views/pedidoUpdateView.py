from django.db import models
from django.db.models import query
from django.db.models.query import QuerySet
from rest_framework import generics,status
from rest_framework.response import Response
from rest_framework.serializers import Serializer
from authApp.models.pedido import Pedido
from authApp.serializers.pedidoSerializer import PedidoSerializer

class PedidoUpdateView(generics.RetrieveAPIView):
    def put (self,request,pk=None,*args,**kawargs):
        queryset = Pedido.objects.filter(id_pedido=pk).first()
        Pedido_Serializer=PedidoSerializer(queryset,data=request.data)
        if queryset:
            if Pedido_Serializer.is_valid():
                Pedido_Serializer.save()
                return Response(Pedido_Serializer.data,status=status.HTTP_200_OK)
        return Response({'message':'No se ha encontrado el Pedido'},status=status.HTTP_400_BAD_REQUEST)        
