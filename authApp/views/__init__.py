from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .userUpdateView import UserUpdateView
from .userDeleteView import UserDeleteView

from .comentarioDetailView import ComentarioDetailView
from .comentarioCreateView import ComentarioCreateView
from .comentarioView import ComentarioView
from .comentarioUpdateView import ComentarioUpdateView
from .comentarioDeleteView import ComentarioDeleteView

from .productoDetailView import ProductoDetailView
from .productoCreateView import ProductoCreateView
from .productoView import ProductoView
from .productoUpdateView import ProductoUpdateView
from .productoDeleteView import ProductoDeleteView

from .pedidoDetailView import PedidoDetailView
from .pedidoCreateView import PedidoCreateView
from .pedidoView import PedidoView
from .pedidoUpdateView import PedidoUpdateView
from .pedidoDeleteView import PedidoDeleteView