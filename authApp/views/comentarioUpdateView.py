from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.comentario import Comentario
from authApp.serializers.comentarioSerializer import ComentarioSerializer


class ComentarioUpdateView(generics.RetrieveAPIView):   

    def put(self,request,pk=None, *args, **kwargs, ):            
        queryset = Comentario.objects.filter(id = pk).first()      
        comentarioSerializer = ComentarioSerializer(queryset,data=request.data)
        
        if queryset:
            if comentarioSerializer.is_valid():
                comentarioSerializer.save()
                return Response(comentarioSerializer.data, status=status.HTTP_200_OK)        
            return Response(comentarioSerializer.errors, status=status.HTTP_400_BAD_REQUEST)        
        return Response({'message':'No se ha encontrado el comentario'}, status=status.HTTP_400_BAD_REQUEST)        

        
        