from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.producto import Producto
from authApp.serializers.productoSerializer import ProductoSerializer


class ProductoView(generics.RetrieveAPIView):

    permission_classes = ()
    queryset = Producto.objects.all()

    serializerclass = ProductoSerializer

    def get(self, request, *args, **kwargs):
        queryset = Producto.objects.all()
        producto_serializer = ProductoSerializer(queryset, many=True)
        return Response(producto_serializer.data, status=status.HTTP_200_OK)
