from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.producto import Producto


class ProductoDeleteView(generics.RetrieveAPIView):   

    def delete(self,request,pk=None, *args, **kwargs, ):            
        queryset = Producto.objects.filter(id = pk).first()              

        if queryset:            
            queryset.delete()
            return Response({'message':'Se eliminó el producto'}, status=status.HTTP_200_OK)                
        return Response({'message':'No se ha encontrado el producto'}, status=status.HTTP_400_BAD_REQUEST)        