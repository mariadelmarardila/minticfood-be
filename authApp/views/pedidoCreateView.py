from rest_framework import views, status
from rest_framework.response import Response
from authApp.serializers.pedidoSerializer import PedidoSerializer

class PedidoCreateView(views.APIView):

    def post(self,request,*args,**kwargs):
        ped_serializer=PedidoSerializer(data=request.data)
        if ped_serializer.is_valid():
            ped_serializer.save()
            return Response({'message':'Pedido Creado Correctamente'},status=status.HTTP_201_CREATED) 
        return Response(ped_serializer.errors,status=status.HTTP_400_BAD_REQUEST)   
