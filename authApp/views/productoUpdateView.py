from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.producto import Producto
from authApp.serializers.productoSerializer import ProductoSerializer


class ProductoUpdateView(generics.RetrieveAPIView):   

    def put(self,request,pk=None, *args, **kwargs, ):            
        queryset = Producto.objects.filter(id = pk).first()      
        producto_serializer = ProductoSerializer(queryset,data=request.data)
        
        if queryset:
            if producto_serializer.is_valid():
                producto_serializer.save()
                return Response(producto_serializer.data, status=status.HTTP_200_OK)        
            return Response(producto_serializer.errors, status=status.HTTP_400_BAD_REQUEST)        
        return Response({'message':'No se ha encontrado el producto'}, status=status.HTTP_400_BAD_REQUEST)        

        
        