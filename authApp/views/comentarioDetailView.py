from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.comentario import Comentario
from authApp.serializers.comentarioSerializer import ComentarioSerializer


class ComentarioDetailView(generics.RetrieveAPIView):   

    def get(self,request,pk=None, *args, **kwargs, ):            
        queryset = Comentario.objects.filter(id = pk).first()      
        comentario_serializer = ComentarioSerializer(queryset)
        
        return Response(comentario_serializer.data, status=status.HTTP_200_OK)        
