from authApp.models.pedido import Pedido
from authApp.serializers.pedidoSerializer import PedidoSerializer
from rest_framework.serializers import Serializer
from rest_framework import generics,status
from rest_framework.response import Response

class PedidoDetailView(generics.RetrieveAPIView):
    def get(self,request,pk=None,*args,**kwargs):
        queryset=Pedido.objects.filter(id_pedido=pk).first()
        pedido_serializer=PedidoSerializer(queryset)
        return Response(pedido_serializer.data,status=status.HTTP_200_OK)
