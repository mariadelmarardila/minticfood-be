from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.comentario import Comentario
from authApp.serializers.comentarioSerializer import ComentarioSerializer


class ComentarioView(generics.RetrieveAPIView):

    def get(self, request,fk=None, *args, **kwargs):
        queryset = Comentario.objects.filter(email_c = fk)
        comentario_serializer = ComentarioSerializer(queryset, many=True)
        return Response(comentario_serializer.data, status=status.HTTP_200_OK)
