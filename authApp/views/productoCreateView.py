from rest_framework import status, views
from rest_framework.response import Response
from authApp.serializers.productoSerializer import ProductoSerializer


class ProductoCreateView(views.APIView):
    
    def post(self, request, *args, **kwargs):
        serializer = ProductoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'message':'Producto creado correctamente'},status=status.HTTP_201_CREATED)
        return Response(serializer.errors,status=status.HTTP_400_BAD_REQUEST)    
        
        

        
