#from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.user import User
from authApp.serializers.userSerializer import UserSerializer
from django.db.models.query import QuerySet


class UserDeleteView(generics.RetrieveAPIView):   

    def delete(self,request,pk=None, *args, **kwargs, ):            
        queryset = User.objects.filter(username = pk).first()      
        if queryset:            
            queryset.delete()
            return Response({'message':'Se eliminó el usuario'}, status=status.HTTP_200_OK)                
        return Response({'message':'No se ha encontrado el usuario'}, status=status.HTTP_400_BAD_REQUEST)  