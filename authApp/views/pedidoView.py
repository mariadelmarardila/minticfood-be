from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.pedido import Pedido
from authApp.serializers.pedidoSerializer import PedidoSerializer

class PedidoView(generics.RetrieveAPIView):

    def get(self, request,fk=None, *args, **kwargs):
        queryset = Pedido.objects.filter(email_c = fk)
        pedido_serializer = PedidoSerializer(queryset, many=True)
        return Response(pedido_serializer.data, status=status.HTTP_200_OK)
