from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.producto import Producto
from authApp.serializers.productoSerializer import ProductoSerializer


class ProductoDetailView(generics.RetrieveAPIView):   

    def get(self,request,pk=None, *args, **kwargs, ):            
        queryset = Producto.objects.filter(id = pk).first()      
        producto_serializer = ProductoSerializer(queryset)
        
        return Response(producto_serializer.data, status=status.HTTP_200_OK)        
