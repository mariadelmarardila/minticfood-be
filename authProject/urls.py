"""authProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from authApp import views

urlpatterns = [
    path('admin/', admin.site.urls),

    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('updateuser/<pk>/', views.UserUpdateView.as_view()),
    path('deleteuser/<pk>/', views.UserDeleteView.as_view()),


    path('allcomentarios/<fk>/', views.ComentarioView.as_view()),
    path('comentarios/', views.ComentarioCreateView.as_view()),
    path('comentarios/<int:pk>/', views.ComentarioDetailView.as_view()),
    path('updatecomentarios/<int:pk>/', views.ComentarioUpdateView.as_view()),
    path('deletecomentarios/<int:pk>/', views.ComentarioDeleteView.as_view()),

    path('allproductos/', views.ProductoView.as_view()),
    path('productos/', views.ProductoCreateView.as_view()),
    path('productos/<int:pk>/', views.ProductoDetailView.as_view()),
    path('updateproductos/<int:pk>/', views.ProductoUpdateView.as_view()),
    path('deleteproductos/<int:pk>/', views.ProductoDeleteView.as_view()),\

    path('allpedidos/<fk>/', views.PedidoView.as_view()),
    path('pedido/', views.PedidoCreateView.as_view()),
    path('pedido/<pk>/', views.PedidoDetailView.as_view()),
    path('uppedido/<pk>/', views.PedidoUpdateView.as_view()),
    path('delpedido/<pk>/', views.PedidoDeleteView.as_view()),
    
]
